﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using IWBotAPI.Model;
using Microsoft.EntityFrameworkCore.Design;
using System.Reflection;

namespace IWBotAPI.Repo
{
    public class IWBotContext : IdentityDbContext<AdminUser>
    {
        public IWBotContext(DbContextOptions<IWBotContext> options) : base(options)
        {
        }



        public DbSet<Employee> Employees { get; set; }
        public DbSet<LeaveDetails> LeaveDetails { get; set; }
        public DbSet<Food> FoodDetails { get; set; }
        public DbSet<FoodCategory> FoodCategories { get; set; }
        public DbSet<FoodOrder> FoodOrders { get; set; }
        public DbSet<Notification> Notifications { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<AdminUser>().ToTable("AdminUser").Property(p => p.Id).HasColumnName("ID");

        }
    }

    public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<IWBotContext>
    {
        public IWBotContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<IWBotContext>();

            builder.UseSqlServer("Data Source=DESKTOP-2R2VIGC;Initial Catalog=IWBotDB;Integrated Security=True;MultipleActiveResultSets=True",
            //builder.UseSqlServer("Data Source=iwbotapidbserver.database.windows.net,1433;Initial Catalog=IWBotAPI_db;Persist Security Info=False;MultipleActiveResultSets=False; User ID=IWBOT;Password=InsightWorkshop@#001",
                optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(IWBotContext).GetTypeInfo().Assembly.GetName().Name));
            return new IWBotContext(builder.Options);
        }
    }
}
