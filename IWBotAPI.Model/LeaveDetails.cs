﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IWBotAPI.Model
{
    public class LeaveDetails
    {
        public Guid ID { get; set; }
        [Required]
        public DateTime LeaveDate { get; set; }
        [Required]
        public string LeaveDuration { get; set; }
        [Required]
        public string LeaveType { get; set; }
        [Required]
        public string LeaveReason { get; set; }
        public int? Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate{ get; set; }
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual AdminUser UserDetails { get; set; }
    }


    public class LeaveViewModel
    {
        public Guid ID { get; set; }
        public string EmpName { get; set; }
        public string EmpID { get; set; }
        public DateTime LeaveDate { get; set; }
        public string LeaveDuration { get; set; }
        public string LeaveType { get; set; }
        public string LeaveReason { get; set; }
        public int? Status { get; set; }
        public int Count { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual AdminUser UserDetails { get; set; }
    }
}
