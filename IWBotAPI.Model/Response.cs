﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace IWBotAPI.Model
{
    public class Response
    {
        [Description("Status code of state of response")]
        public int Code { get; set; }
        [Description("Response Message")]
        public string Message { get; set; }
        [Description("Container of response Data")]
        public dynamic Data { get; set; }
    }
}
