﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IWBotAPI.Model
{
    public class Notification
    {
        public Guid ID { get; set; }
        [Required]
        public string Message { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}
