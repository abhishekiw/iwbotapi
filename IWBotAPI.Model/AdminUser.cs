﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IWBotAPI.Model
{
    public class AdminUser: IdentityUser
    {
       [Required]
        public DateTime? DOB { get; set; }
    }

}
