﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace IWBotAPI.Model
{
    public class FoodCategory
    {
        public Guid ID { get; set; }
        [Required]
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public class FoodCategoryModel
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
    }
}
