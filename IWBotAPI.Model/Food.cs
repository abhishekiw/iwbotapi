﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IWBotAPI.Model
{
    public class Food
    {
        public Guid ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal Price { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [Required]
        public Guid FoodCategoryID { get; set; }
        [ForeignKey("FoodCategoryID")]
        public virtual FoodCategory FoodCategoryDetails { get; set; }
    }

    public class FoodViewModel
    {
        public Guid? ID { get; set; }
        public string Name { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public decimal Price { get; set; }

        public string FoodCategoryName { get; set; }
        public Guid FoodCategoryID { get; set; }
        [ForeignKey("FoodCategoryID")]
        public virtual FoodCategory FoodCategoryDetails { get; set; }
    }
}
