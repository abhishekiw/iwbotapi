﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IWBotAPI.Model
{
    public class FoodOrder
    {
        public Guid ID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        [Required]
        public Guid FoodID { get; set; }
        [ForeignKey("FoodID")]
        public virtual Food FoodDetails { get; set; }
        [Required]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual AdminUser UserDetails { get; set; }
        [Required]
        public int Quantity { get; set; }
        public string Description { get; set; }
    }


    public class FoodOrderViewModel
    {
        public Guid ID { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public Guid FoodID { get; set; }
        [ForeignKey("FoodID")]
        public virtual Food FoodDetails { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual AdminUser UserDetails { get; set; }

        public int Quantity { get; set; }

        public string EmpName { get; set; }
        public string FoodName { get; set; }
        public string Description { get; set; }
    }

}
