﻿using IWBotAPI.Model;
using IWBotAPI.Repo;
using IWBotAPI.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWBotAPI.Service.Implementation
{
    public class EmployeeService : IEmployeeService
    {

        private IWBotContext _context;
        public EmployeeService(IWBotContext context)
        {
            _context = context;
        }

        //get employee
        public IQueryable<Employee> GetEmployees()
        {
            try
            {
                var empl =
                from emp in _context.Employees
                select new Employee()
                {
                    Email = emp.Email,
                    DOB = emp.DOB,
                    Gender = emp.Gender,
                    CreatedDate = emp.CreatedDate,
                    ID = emp.ID,
                    ModifiedDate = emp.ModifiedDate
                };
                return empl.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //post employee
        public async Task SaveEmployee(Employee employee)
        {
            try
            {
                employee.CreatedDate = DateTime.Now;
                employee.ModifiedDate = DateTime.Now;

                _context.Employees.Add(employee);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
