﻿using IWBotAPI.Model;
using IWBotAPI.Repo;
using IWBotAPI.Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace IWBotAPI.Service.Implementation
{
    public class NotificationService: INotificationService
    {
        private IWBotContext _context;
        public NotificationService(IWBotContext context)
        {
            _context = context;
        }

        //get notification
        public IQueryable<Notification> GetNotifications()
        {
            var notification =
                 from msg in _context.Notifications
                 select new Notification()
                 {
                     ID = msg.ID,
                     Message = msg.Message,
                     CreatedDate = msg.CreatedDate
                 };
            return notification.AsQueryable();
        }


        //send notification
        public async Task SendNotification(Notification notification)
        {
            try
            {
                if (notification.Message != "")
                {
                    notification.CreatedDate = DateTime.Now;
                    notification.ModifiedDate = DateTime.Now;

                    _context.Notifications.Add(notification);
                    await _context.SaveChangesAsync();
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        //delete notification
        public async Task NotificationDelete(Guid id)
        {
            Notification notification = _context.Notifications.Find(id);
            var notifications = await _context.Notifications.FindAsync(id);
            _context.Notifications.Remove(notification);
            await _context.SaveChangesAsync();
        }
    }
}
