﻿using IWBotAPI.Model;
using IWBotAPI.Repo;
using IWBotAPI.Service.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace IWBotAPI.Service.Implementation
{
   
    public class LeaveService: ILeaveService
    {
        

        #region leave constructor
        private IWBotContext _context;
        public LeaveService(IWBotContext context)
        {
            _context = context;
        }
        #endregion


        #region Get leave details
        public IQueryable<LeaveViewModel> GetLeaveDetails()
        {
            var lDetail =
               from leave in _context.LeaveDetails
               join emp in _context.Users on leave.UserId equals emp.Id
               orderby leave.LeaveDate ascending
               select new LeaveViewModel()
               {
                   ID = leave.ID,
                   EmpName = emp.Email,
                   EmpID = emp.Id,
                   LeaveDate = leave.LeaveDate,
                   LeaveDuration = leave.LeaveDuration,
                   LeaveType = leave.LeaveType,
                   LeaveReason = leave.LeaveReason,
                   Status = leave.Status

               };
            return lDetail.AsQueryable();
        }
        #endregion

        #region Post leave
        public async Task SaveLeave(LeaveDetails leaveDetails)
        {
            var leave = new LeaveDetails();
            leave.CreatedDate = DateTime.Now;
            leave.ModifiedDate = DateTime.Now;
            //leave.EmployeeID = leaveDetails.EmployeeID;
            
            _context.LeaveDetails.Add(leaveDetails);
            await _context.SaveChangesAsync();
        }

        #endregion

        #region review leave
        public async Task ReviewLeave(LeaveDetails leaveDetails, Guid id)
        {
            var leave = _context.LeaveDetails.ToList().Where(x => x.ID == id).FirstOrDefault();
            leave.Status = leaveDetails.Status;
            leave.ModifiedDate = DateTime.Now;
            _context.Entry(leave).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        #endregion
    }
}
