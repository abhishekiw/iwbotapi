﻿using IWBotAPI.Model;
using IWBotAPI.Repo;
using IWBotAPI.Service.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWBotAPI.Service.Implementation
{
    public class FoodService : IFoodService
    {
        #region Constructor
        private IWBotContext _context;
        public FoodService(IWBotContext context)
        {
            _context = context;
        }
        #endregion


        #region Get Food Details
        //get food details
        public IQueryable<FoodViewModel> GetFoodDetails()
        {
            try
            {
                var fDetail =
            from food in _context.FoodDetails
            join fCat in _context.FoodCategories on food.FoodCategoryID equals fCat.ID
            orderby fCat.Name ascending
            select new FoodViewModel
            {
                ID = food.ID,
                FoodCategoryName = fCat.Name,
                Name = food.Name,
                Price = food.Price
            };

                return fDetail.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region Post food menu
        //save food
        public async Task SaveFood(Food Record)
        {
            try
            {
                Record.CreatedDate = DateTime.Now;
                Record.ModifiedDate = DateTime.Now;
                _context.FoodDetails.Add(Record);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region Delete food menu
        //delete food
        public async Task FoodDelete(Guid id)
        {
            try
            {
                Food foods = _context.FoodDetails.Find(id);
                var food = await _context.FoodDetails.FindAsync(id);
                _context.FoodDetails.Remove(food);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region Get Food Categories
        //  food categories
        public IEnumerable<object> GetFoodCategories()
        {
            try
            {
                var cDetail =
             from fcat in _context.FoodCategories
             orderby fcat.Name ascending
             select new
             {
                 fcat.ID,
                 fcat.Name
             };
                return cDetail.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region PostFoodCategory

        //post food category
        public async Task SaveCategory(FoodCategory foodCategory)
        {
            try
            {

                foodCategory.CreatedDate = DateTime.Now;
                foodCategory.ModifiedDate = DateTime.Now;

                _context.FoodCategories.Add(foodCategory);
                await _context.SaveChangesAsync();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion


        #region Delete Category
        public async Task CategoryDelete(Guid id)
        {
            try
            {

                FoodCategory foodCategory = _context.FoodCategories.Find(id);
                var category = await _context.FoodCategories.FindAsync(id);
                _context.FoodCategories.Remove(foodCategory);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion


        #region Get Food order
        //  food order
        public IQueryable<object> GetFoodOrders()
        {
            try
            {
                var oDetail =
                from order in _context.FoodOrders
                join food in _context.FoodDetails on order.FoodID equals food.ID
                join emp in _context.Users on order.UserId equals emp.Id
                orderby order.CreatedDate ascending
                select new
                {
                    order.ID,
                    EmpName = emp.Email,
                    FoodName = food.Name,
                    order.Quantity,
                    order.Description
                };
                return oDetail.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region Daily order 
        //get daily orders
        public IQueryable<object> GetDailyFoodOrders()
        {
            try
            {
                var dailyOrder =
               from order in _context.FoodOrders
               join food in _context.FoodDetails on order.FoodID equals food.ID
               join emp in _context.Users on order.UserId equals emp.Id
               where order.CreatedDate == DateTime.Now.Date
               select new
               {
                   order.ID,
                   EmpName = emp.Email,
                   FoodName = food.Name,
                   order.Quantity,
                   order.Description
               };
                return dailyOrder.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion


        #region Save food order
        public async Task SaveOrder(FoodOrder foodOrder)
        {
            try
            {
                foodOrder.CreatedDate = DateTime.Now.Date;
                foodOrder.ModifiedDate = DateTime.Now.Date;
                _context.FoodOrders.Add(foodOrder);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region Delete Food Order
        public async Task OrderDelete(Guid id)
        {
            try
            {
                FoodOrder foodOrders = _context.FoodOrders.Find(id);
                var foodOrder = await _context.FoodOrders.FindAsync(id);
                _context.FoodOrders.Remove(foodOrders);
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

    }
}
