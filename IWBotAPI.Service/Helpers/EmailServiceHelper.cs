﻿using Microsoft.AspNet.Identity;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
namespace IWBotAPI.Service.Helpers
{
    public class EmailServiceHelper : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
            MailMessage msg = new MailMessage();
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            msg.Subject = message.Subject;
            msg.Body = message.Body;
            msg.From = new MailAddress(ConfigurationManager.AppSettings["DefaultEmail"].ToString());
            msg.To.Add(message.Destination);
            msg.IsBodyHtml = true;
            msg.Priority = MailPriority.High;
            client.Host = ConfigurationManager.AppSettings["DefaultGateway"].ToString();
            System.Net.NetworkCredential basicauthenticationinfo = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["DefaultEmail"].ToString(), ConfigurationManager.AppSettings["DefaultEmailPassword"].ToString());
            client.Port = int.Parse(ConfigurationManager.AppSettings["DefaultSMTPPort"].ToString());
            var ssl = ConfigurationManager.AppSettings["DefaultEmailSSL"].ToString();
            if (ssl == "true")
                client.EnableSsl = true;
            else
                client.EnableSsl = false;
            client.UseDefaultCredentials = false;
            client.Credentials = basicauthenticationinfo;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            await client.SendMailAsync(msg);
        }

    }
}