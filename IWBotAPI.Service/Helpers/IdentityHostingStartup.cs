﻿//using IWBotAPI.Model;
//using IWBotAPI.Repo;
//using Microsoft.AspNetCore.Hosting;
//using Microsoft.AspNetCore.Identity;
//using System;
//using System.Collections.Generic;
//using System.Text;

//namespace IWBotAPI.Service.Helpers
//{
//    public class IdentityHostingStartup : IHostingStartup
//    {
//        public void Configure(IWebHostBuilder builder)
//        {
//            builder.ConfigureServices((context, services) => {
//                services.AddDbContext<IWBotContext>(options =>
//                    options.UseSqlServer(
//                        context.Configuration.GetConnectionString("WebPWrecoverContextConnection")));

//                services.AddDefaultIdentity<AdminUser>(config =>
//                {
//                    config.SignIn.RequireConfirmedEmail = true;
//                })
//                    .AddEntityFrameworkStores<IWBotContext>();
//            });
//        }
//    }
//}
