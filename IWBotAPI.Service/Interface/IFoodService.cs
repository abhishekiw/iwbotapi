﻿using IWBotAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWBotAPI.Service.Interface
{
    public interface IFoodService    {
        #region Food Menu
        IQueryable<FoodViewModel> GetFoodDetails();
        Task SaveFood(Food Record);
        Task FoodDelete(Guid id);
        IQueryable<object> GetDailyFoodOrders();
        #endregion

        #region Food Categories
        // food categories
        IEnumerable<object> GetFoodCategories();

        Task SaveCategory(FoodCategory foodCategory);

        Task CategoryDelete(Guid id);
        #endregion

        #region Food Orders
        // food orders
        IQueryable<object> GetFoodOrders();

        Task SaveOrder(FoodOrder foodOrder);

        Task OrderDelete(Guid id);
        #endregion

    }
}
