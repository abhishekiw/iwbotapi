﻿using IWBotAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWBotAPI.Service.Interface
{
    public interface IEmployeeService
    {
        IQueryable<Employee> GetEmployees();
        Task SaveEmployee(Employee employee); 
    }
}
