﻿using IWBotAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWBotAPI.Service.Interface
{
    public interface INotificationService
    {
        IQueryable<Notification> GetNotifications();

        Task SendNotification(Notification notification);

        Task NotificationDelete(Guid id);
    }
}
