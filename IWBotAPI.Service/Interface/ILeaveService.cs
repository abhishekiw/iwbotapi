﻿using IWBotAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IWBotAPI.Service.Interface
{
    public interface ILeaveService
    {
        IQueryable<LeaveViewModel> GetLeaveDetails();

        Task SaveLeave(LeaveDetails leaveDetails);
        Task ReviewLeave(LeaveDetails leaveDetails, Guid id);
    }
}
