﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IWBotAPI.Model;
using IWBotAPI.Repo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using IWBotAPI.Service.Interface;

namespace IWBotAPI.Controllers
{
    [EnableCors("IWBotCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]

    public class NotificationsController : ControllerBase
    {
        private readonly IWBotContext _context;
        private INotificationService _notificationService;

        public NotificationsController(IWBotContext context, INotificationService notificationService)
        {
            _notificationService = notificationService;
            _context = context;
        }

        // GET: api/Notifications
        [HttpGet]
        public IQueryable<Notification> GetNotifications()
        {
            try
            {

                var notification =
                    _notificationService.GetNotifications();
                return notification.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

       

        // POST: api/Notifications
        [HttpPost]
        public async Task<IActionResult> PostNotification([FromBody] Notification notification)
        {
            try
            {
                await _notificationService.SendNotification(notification);
                return Ok(notification);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // DELETE: api/Notifications/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteNotification([FromRoute] Guid id)
        {
            var notification = await _context.Notifications.FindAsync(id);
            if (notification == null)
            {
                return NotFound();
            }
            try
            {
                await _notificationService.NotificationDelete(id);

            }
            catch (Exception e)
            {
                throw e;
            }

            return Ok(notification);
        }

        private bool NotificationExists(Guid id)
        {
            return _context.Notifications.Any(e => e.ID == id);
        }
    }
}