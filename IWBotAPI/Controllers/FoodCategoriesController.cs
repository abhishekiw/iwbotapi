﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IWBotAPI.Model;
using IWBotAPI.Repo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using IWBotAPI.Service.Interface;
using System.Text.RegularExpressions;

namespace IWBotAPI.Controllers
{
    [EnableCors("IWBotCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class FoodCategoriesController : ControllerBase
    {
        private readonly IWBotContext _context;
        private IFoodService _foodService;
        public FoodCategoriesController(IWBotContext context, IFoodService foodService)
        {
            _foodService = foodService;
            _context = context;
        }

        // GET: api/FoodCategories
        [HttpGet]
        public IEnumerable<object> GetFoodCategories()
        {
            try
            {
                var cDetail =
             _foodService.GetFoodCategories();
                return cDetail.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // POST: api/FoodCategories
        [HttpPost]
        public async Task<IActionResult> PostFoodCategory([FromBody] FoodCategory foodCategory)
        {
            string input = foodCategory.Name;
            Match match = Regex.Match(input, @"^[a-zA-Z ]*$",
          RegexOptions.IgnoreCase);
            try
            {
                if (match.Success)
                {
                    await _foodService.SaveCategory(foodCategory);
                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Ok(foodCategory);
        }

        // DELETE: api/FoodCategories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFoodCategory([FromRoute] Guid id)
        {
            var foodCategory = await _context.FoodCategories.FindAsync(id);
            if (foodCategory == null)
            {
                return NotFound();
            }
            try
            {
                await _foodService.CategoryDelete(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(foodCategory);
        }

        private bool FoodCategoryExists(Guid id)
        {
            return _context.FoodCategories.Any(e => e.ID == id);
        }
    }
}