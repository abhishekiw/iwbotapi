﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IWBotAPI.Model;
using IWBotAPI.Repo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using IWBotAPI.Service.Interface;
using System.Security.Claims;

namespace IWBotAPI.Controllers
{
    [EnableCors("IWBotCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class LeaveDetailsController : ControllerBase
    {
        private readonly ClaimsPrincipal _caller;
        #region leave controller constructor
        private readonly IWBotContext _context;
        private ILeaveService _leaveService;
        public LeaveDetailsController(IWBotContext context, ILeaveService leaveService, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _leaveService = leaveService;
            _caller = httpContextAccessor.HttpContext.User;
        }
        #endregion


        #region get leave details
        /// <summary>
        /// Get all the value from leave details table
        /// </summary>
        /// <returns></returns>
        // GET: api/LeaveDetails
        [HttpGet]
        public IQueryable<LeaveViewModel> GetLeaveDetails()
        {
            try
            {
                var lDetail = _leaveService.GetLeaveDetails();

                return lDetail.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region count total leave
        /// <summary>
        /// number of total leave taken by employee
        /// </summary>
        /// <returns></returns>
        [Route("totalLeave")]
        [HttpGet]
        public int GetTotalLeave()
        {
            try
            {
                var leaveCount = _context.LeaveDetails.ToList().Count();
                return leaveCount;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region review leave
        /// <summary>
        /// review employee leave and change status
        /// </summary>
        /// <param name="id"></param>
        /// <param name="leaveDetails"></param>
        /// <returns></returns>
        // PUT: api/LeaveDetails/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutLeaveDetails([FromRoute] Guid id, [FromBody] LeaveDetails leaveDetails)
        {
            

            if (id != leaveDetails.ID)
            {
                return BadRequest();
            }

            try
            {
                await _leaveService.ReviewLeave(leaveDetails, id);
            }
            catch (DbUpdateConcurrencyException e)
            {
                if (!LeaveDetailsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw e;
                }
            }
            return Ok(leaveDetails);
        }
        #endregion


        #region save leave 
        /// <summary>
        /// Post leave
        /// </summary>
        /// <param name="leaveDetails"></param>
        /// <returns></returns>
        // POST: api/LeaveDetails
        [HttpPost]
        public async Task<IActionResult> PostLeaveDetails([FromBody] LeaveDetails leaveDetails)
        {
            try
            {
                //var userId = _caller.Claims.SingleOrDefault(c => c.Type == "id");
                
                await _leaveService.SaveLeave(leaveDetails);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok(leaveDetails);
        }
        #endregion

        private bool LeaveDetailsExists(Guid id)
        {
            return _context.LeaveDetails.Any(e => e.ID == id);
        }
    }
}