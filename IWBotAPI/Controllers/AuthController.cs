﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IWBotAPI.Model;
using IWBotAPI.Models.AccountViewModels;
using IWBotAPI.Repo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using AutoMapper;
using IWBotAPI.Models;
using IWBotAPI.Service.Interface;
using Microsoft.Extensions.Options;
using System.Net;

namespace IWBotAPI.Controllers
{
    [Route("[controller]/[action]")]
    public class AuthController : Controller
    {
        private readonly SignInManager<AdminUser> _signInManager;
        private readonly UserManager<AdminUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IWBotContext _iWBotContext;
        private readonly IMapper _mapper;

        public AuthController(
            UserManager<AdminUser> userManager,
            SignInManager<AdminUser> signInManager,
            IConfiguration configuration,
            IMapper mapper,
            IWBotContext iWBotContext
            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _iWBotContext = iWBotContext;
            _mapper = mapper;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public Response HttpResponse(int statusCode, string msg, object data)
        {
            return new Response
            {
                Code = statusCode,
                Message = msg,
                Data = data
            };
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public Response HttpResponse(int statusCode, string msg)
        {
            return new Response
            {
                Code = statusCode,
                Message = msg
            };
        }

        // POST api/auth/login
        [HttpPost]
        public async Task<object> Post([FromBody]LoginViewModel model) 
        {

            var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, false);

            if (result.Succeeded)
            {
                //var user = new AdminUser();
                var appUser = _userManager.Users.SingleOrDefault(r => r.Email == model.Email);
                var role = _userManager.GetRolesAsync(appUser);
                var token = await GenerateJwtToken(model.Email, appUser);


                string var = role.Result[0].ToString();
                return HttpResponse((int)HttpStatusCode.OK, var, token);
            }

            throw new ApplicationException("INVALID_LOGIN_ATTEMPT");
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterViewModel model)
        {
            try
            {
                var user = new AdminUser()
                {
                    UserName = model.Email,
                    Email = model.Email,
                    DOB = DateTime.Parse(model.DOB)
                    
                };
                IdentityResult results = await _userManager.CreateAsync(user, model.Password);
                await _userManager.AddToRoleAsync(user, "Employee");
                return Ok(results);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private async Task<object> GenerateJwtToken(string email, IdentityUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_configuration["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }
}