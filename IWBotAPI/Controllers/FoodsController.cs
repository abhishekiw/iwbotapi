﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IWBotAPI.Model;
using IWBotAPI.Repo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using IWBotAPI.Service.Interface;
using IWBotAPI.Service.Implementation;
using System.Text.RegularExpressions;

namespace IWBotAPI.Controllers
{
    [EnableCors("IWBotCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class FoodsController : ControllerBase
    {
        private readonly IWBotContext _context;
        private IFoodService _foodService;

        public FoodsController(IFoodService foodService, IWBotContext context)
        {
            _context = context;
            _foodService = foodService;
        }

        // GET: api/Foods
        [HttpGet]
        public IQueryable<FoodViewModel> GetFoodDetails()
        {
            try
            {
                var fDetail =
              _foodService.GetFoodDetails();
                return fDetail.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        // PUT: api/Foods/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFood([FromRoute] Guid id, [FromBody] Food food)
        {

            if (id != food.ID)
            {
                return BadRequest();
            }

            _context.Entry(food).State = EntityState.Modified;

            try
            {
                food.ModifiedDate = DateTime.Now.Date;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FoodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Foods
        [HttpPost]
        public async Task<IActionResult> PostFood([FromBody] Food food)
        {
            try
            {
                if (food.FoodCategoryID != Guid.Empty)
                {
                    await _foodService.SaveFood(food);

                }
                else
                {
                    return BadRequest();
                }

            }
            catch (Exception e)
            {
                throw e;
            }

            return Ok(food);
        }

        // DELETE: api/Foods/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFood([FromRoute] Guid id)
        {

            var food = await _context.FoodDetails.FindAsync(id);
            if (food == null)
            {
                return NotFound();
            }
            try
            {
                await _foodService.FoodDelete(id);
            }
            catch (Exception e)
            {
                throw e;
            }

            return Ok(food);
        }

        private bool FoodExists(Guid id)
        {
            return _context.FoodDetails.Any(e => e.ID == id);
        }
    }
}