﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IWBotAPI.Model;
using IWBotAPI.Repo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using IWBotAPI.Service.Interface;

namespace IWBotAPI.Controllers
{
    [EnableCors("IWBotCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]

    public class FoodOrdersController : ControllerBase
    {
        private readonly IWBotContext _context;
        private IFoodService _foodService;

        #region Constructor
        public FoodOrdersController(IWBotContext context, IFoodService foodService)
        {
            _foodService = foodService;
            _context = context;
        }
        #endregion


        #region Get Food Orders
        // GET: api/FoodOrders
        /// <summary>
        /// Get food orders 
        /// </summary>
        /// <returns></returns>
        [HttpGet]     
        public IQueryable<object> GetFoodOrders()
        {
            try
            {
                var oDetail =
               _foodService.GetFoodOrders();
                return oDetail.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region daily order count
        /// <summary>
        /// Get total number of order per day (count)
        /// </summary>
        /// <returns></returns>
        [Route("totalOrder")]
        [HttpGet]
        public int GetTotalOrder()
        {
            try
            {
                var orderCount = _context.FoodOrders.Where(x => x.CreatedDate == DateTime.Now.Date).ToList().Count();
                return orderCount;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Get daily food orders
        // GET: api/FoodOrders/DailyOrders
        /// <summary>
        /// Get daily orders of food
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("DailyOrders")]
        public IQueryable<object> GetDailyFoodOrders()
        {
            try
            {
                var dailyOrder =
               _foodService.GetDailyFoodOrders();
                return dailyOrder.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region Update food orders
        // PUT: api/FoodOrders/5
        /// <summary>
        /// update food orders
        /// </summary>
        /// <param name="id"></param>
        /// <param name="foodOrder"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFoodOrder([FromRoute] Guid id, [FromBody] FoodOrder foodOrder)
        {
            if (id != foodOrder.ID)
            {
                return BadRequest();
            }

            _context.Entry(foodOrder).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FoodOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        #endregion


        #region Post food orders
        // POST: api/FoodOrders
        /// <summary>
        /// Saves new food orders
        /// </summary>
        /// <param name="foodOrder"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostFoodOrder([FromBody] FoodOrder foodOrder)
        {
            try
            {
                await _foodService.SaveOrder(foodOrder);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return Ok(foodOrder);
        }
        #endregion


        #region Delete orders
        // DELETE: api/FoodOrders/5
        /// <summary>
        /// deletes food orders
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFoodOrder([FromRoute] Guid id)
        {
            var foodOrder = await _context.FoodOrders.FindAsync(id);
            if (foodOrder == null)
            {
                return NotFound();
            }

            try
            {
                await _foodService.OrderDelete(id);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return Ok(foodOrder);
        }
        #endregion

        private bool FoodOrderExists(Guid id)
        {
            return _context.FoodOrders.Any(e => e.ID == id);
        }
    }
}