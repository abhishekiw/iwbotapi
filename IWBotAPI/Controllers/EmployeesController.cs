﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IWBotAPI.Model;
using IWBotAPI.Repo;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using IWBotAPI.Service.Interface;

namespace IWBotAPI.Controllers
{
    [EnableCors("IWBotCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]

    [Authorize]

    public class EmployeesController : ControllerBase
    {

        private readonly IWBotContext _context;
        private IEmployeeService _employeeService;
        public EmployeesController(IWBotContext context, IEmployeeService employeeService)
        {
            _context = context;
            _employeeService = employeeService;
        }

        // GET: api/Employees
        /// <summary>
        /// GET employee details
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IQueryable<Employee> GetEmployees()
        {
            try
            {
                var empl =
               _employeeService.GetEmployees();
                return empl.AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [Route("totalEmployee")]
        [HttpGet]
        public int GetTotalEmployee()
        {
            try
            {
                var empCount = _context.UserRoles.Where(x=> x.RoleId == "E1552FC9-9417-490B-BAE8-5668F0704B33").ToList().Count();
                return empCount;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        // POST: api/Employees
        [HttpPost]
        public async Task<IActionResult> PostEmployee([FromBody] Employee employee)
        {
            try
            {
                await _employeeService.SaveEmployee(employee);
            }
            catch (Exception e)
            {
                throw e;
            }

            return Ok(employee);
        }

        

        private bool EmployeeExists(Guid id)
        {
            return _context.Employees.Any(e => e.ID == id);
        }
    }
}